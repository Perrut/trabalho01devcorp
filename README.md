# Trabalho 01 - Desenvolvimento de Sistemas Corporativos 2020-01 UFF

Implemente uma aplicação web que funciona como um calculadora. 

A aplicação deve apresentar um formulário (Servlet) para que o usuário preencha 2 operadores e selecione uma operação (soma, subtração, multiplicação, divisão e outras que julgar necessário). 

Ao apresentar o resultado da operação (com JSP), a aplicação deve mostrar também os operandos e o número de vezes em que o usuário acessou o aplicativo (utilize um cookie para isso).
