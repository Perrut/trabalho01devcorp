<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<title>Calculadora</title>
</head>
<body>
	<div align='center'>
		<br /> <h1>Calculadora</h1> <br />
		<br /> 
		<p>
			<%
				try {
					Cookie[] cookies = request.getCookies();
					String acessos = "";
					if (cookies != null) {
						for (int i = 0; i < cookies.length; i++) {
							Cookie cookie = cookies[i];
							if (cookie.getName().equals("acessos"))
								acessos = cookie.getValue();
						}
					}
					
					Double resultado = 0d;
					Double operando1 = Double.parseDouble(request.getParameter("operando1"));
					Double operando2 = Double.parseDouble(request.getParameter("operando2"));
					String operando = (String) request.getParameter("operando");
					
					switch (operando) {
					case "sum":
						resultado = operando1 + operando2;
						operando = "+";
						break;
					case "sub":
						resultado = operando1 - operando2;
						operando = "-";
						break;
					case "tim":
						resultado = operando1 * operando2;
						operando = "*";
						break;
					case "div":
						resultado = operando1 / operando2;
						operando = "/";
						break;
					case "exp":
						resultado = Math.pow(operando1, operando2);
						operando = "^";
						break;
					default:
						throw new IllegalArgumentException("Operando inválido.");
					}
					out.println("<p>" + operando1 + " " + operando + " " + operando2 + " = " + resultado + "</p>");
					out.println("<br><p>Quantidade de acessos: " + acessos + "</p>");
				} catch (Exception e) {
					out.println("<p>Falha ao executar operação.</p>");
				}
			%>
		</p>
		<br>
		<p><a href="calculadora">Voltar para o início</a></p>
	</div>
</body>
</html>
