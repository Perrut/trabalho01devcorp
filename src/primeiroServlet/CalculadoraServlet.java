package primeiroServlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Objects;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CalculadoraServlet extends HttpServlet {
	private static final long serialVersionUID = 3296253606648631984L;

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		response.setContentType("text/html;charset=UTF-8");
		
		Cookie[] cookies = request.getCookies();
		String acessos = "";
		if (Objects.nonNull(cookies)) {
			for (int i = 0; i < cookies.length; i++) {
				Cookie cookie = cookies[i];
				if (cookie.getName().equals("acessos"))
					acessos = cookie.getValue();
			}
		}
		Integer acessosNovos = acessos.isEmpty() ? 1 : Integer.parseInt(acessos) + 1;
		Cookie cookie = new Cookie("acessos", acessosNovos.toString());
		response.addCookie(cookie);
		
		PrintWriter out = response.getWriter();
		out.println("<!DOCTYPE html>\n"
				+ "<html lang=\"pt-br\">\n"
				+ "\n"
				+ "<head>\n"
				+ "  <title>Calculadora</title>\n"
				+ "  <style>\n"
				+ "    * {\n"
				+ "      box-sizing: border-box;\n"
				+ "    }\n"
				+ "\n"
				+ "    html,\n"
				+ "    body {\n"
				+ "      width: 100%;\n"
				+ "      height: 100%;\n"
				+ "    }\n"
				+ "\n"
				+ "    body {\n"
				+ "      margin: 0;\n"
				+ "    }\n"
				+ "\n"
				+ "    .form-container {\n"
				+ "      display: flex;\n"
				+ "      flex-direction: column;\n"
				+ "      align-items: center;\n"
				+ "      justify-content: center;\n"
				+ "      height: 100%;\n"
				+ "    }\n"
				+ "\n"
				+ "    form {\n"
				+ "      width: 300px;\n"
				+ "    }\n"
				+ "\n"
				+ "    form>label,\n"
				+ "    form>input,\n"
				+ "    form>select {\n"
				+ "      display: block;\n"
				+ "      width: 100%;\n"
				+ "      margin-bottom: 10px;\n"
				+ "    }\n"
				+ "  </style>\n"
				+ "</head>\n"
				+ "\n"
				+ "<body>\n"
				+ "  <div class=\"form-container\">\n"
				+ "    <h1>Calculadora</h1>\n"
				+ "    <form action=\"calculadora\" method=\"post\">\n"
				+ "      <label for=\"operando1\">Primeiro operando:</label>\n"
				+ "      <input type=\"number\" id=\"operando1\" name=\"operando1\" required>\n"
				+ "\n"
				+ "      <label for=\"operando2\">Segundo operando:</label>\n"
				+ "      <input type=\"number\" id=\"operando2\" name=\"operando2\" required>\n"
				+ "\n"
				+ "      <label for=\"operando\">Operacao:</label>\n"
				+ "      <select name=\"operando\" id=\"operando\">\n"
				+ "        <option value=\"sum\">+</option>\n"
				+ "        <option value=\"sub\">-</option>\n"
				+ "        <option value=\"tim\">*</option>\n"
				+ "        <option value=\"div\">/</option>\n"
				+ "        <option value=\"exp\">^</option>\n"
				+ "      </select>\n"
				+ "\n"
				+ "      <input type=\"submit\" value=\"Calcular\">\n"
				+ "    </form>\n"
				+ "  </div>\n"
				+ "</body>\n"
				+ "\n"
				+ "</html>");
		
		out.close();
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		RequestDispatcher encaminhador = request.getRequestDispatcher("/resultado.jsp");

		if (encaminhador != null)
			encaminhador.forward(request, response);
	}
}